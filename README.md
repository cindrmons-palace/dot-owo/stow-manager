# Stow Manager

a dotFile Manager using `stow` under the hood.

> This project is still a work in progress.

## How to Setup Dev Environment

### Prerequisites

- [go](https://go.dev/)
- [cobra-cli](https://cobra.dev/)
- [gnu-stow](https://www.gnu.org/software/stow/)

### Setup

> Presuming that you have prerequisites installed

1. Clone Project
```
# using https
git clone https://gitlab.com/cindrmons-palace/dot-moe/stow-manager.git

# using ssh 
git clone git@gitlab.com:cindrmons-palace/dot-moe/stow-manager.git
```

2. `cd` into project
```
cd stow-manager
```

3. Create a current build and install all packages
> NOTE: I set binaries not on the root folder, but under the `./out` folder instead.
```
go build -o ./out/
```

4. Tidy some stuff
```
go mod tidy
```

> It is **recommended** to build the program rather than run it, as running it as `go run .` will contain directory problems, as it uses the `os.Executable()` function, which will get the directory of where it is executed, and is the most likely solution of me getting the closest resemblance to `__dirname` on go. Running as is will bring the executable file into a `/tmp/...` folder and will recognise all directories on that directory. 
