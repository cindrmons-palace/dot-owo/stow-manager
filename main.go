/*
Copyright © 2022 cindrmon

*/
package main

import "stow-manager/cmd"

func main() {
	cmd.Execute()
}
